import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';

export default class Layout extends Component {
    render() {
        return (
            <div>
                <aside id="left-panel" className="left-panel">
                    <nav className="navbar navbar-expand-sm navbar-default">
                        <div id="main-menu" className="main-menu collapse navbar-collapse">
                            <ul className="nav navbar-nav">
                                <li>
                                    <NavLink activeClassName="is-active" activeStyle={{ color: '#007bff' }} to="/gotocky"><i className="menu-icon fas fa-keyboard" />Gõ tốc ký </NavLink>
                                </li>
                                <li>
                                    <a activeClassName="is-active" activeStyle={{ color: '#007bff' }} href="/chitietquytac"> <i className="menu-icon fas fa-info-circle" />Chi tiết bộ quy tắc gõ</a>
                                </li>

                                <li>
                                    <NavLink activeClassName="is-active" activeStyle={{ color: '#007bff' }} to="/hocquytac"><i className="menu-icon fas fa-file-alt" /> Học quy tắc</NavLink>
                                </li>
                                <li>
                                    <NavLink activeClassName="is-active" activeStyle={{ color: '#007bff' }} to="/chuyendoikytu"> <i className="menu-icon fas fa-arrows-alt-h" />Chuyển đổi ký tự</NavLink>
                                </li>
                                <li>
                                    <a href="/quanlutudien" className="dropdown-toggle" > <i className="menu-icon fa fa-table" />Quản lý bộ từ điển</a>
                                </li>
                                <li>
                                    <NavLink activeClassName="is-active" activeStyle={{ color: '#007bff' }} to="/tracnghiemquytac"><i class="menu-icon fa fa-graduation-cap" aria-hidden="true" />Trắc nghiệm quy tắc</NavLink>
                                </li>
                                <li>
                                    <NavLink activeClassName="is-active" activeStyle={{ color: '#007bff' }} to="/luyentap"><i className="menu-icon fa fa-puzzle-piece" />Luyện tập</NavLink>
                                </li>
                                <li>
                                    <NavLink activeClassName="is-active" activeStyle={{ color: '#007bff' }} to="https://drive.google.com/file/d/1xkT2hjZAR53X6PYKAPtoBP5iDjGzZ7pD/view" target="_blank" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="menu-icon fa fa-tasks" />Hướng dẫn sử dụng</NavLink>
                                </li>
                                <li>
                                    <NavLink activeClassName="is-active" activeStyle={{ color: '#007bff' }} to="/thongke"> <i className="menu-icon fa fa-bar-chart" />Thống kê</NavLink>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </aside>

                <div id="right-panel" className="right-panel">
                    <header id="header" className="header">
                        <div className="top-left">
                            <div className="navbar-header">
                                <a className="navbar-brand" href="./"><img src="images/logotocky2.png" style={{ maxHeight: '41px' }} alt="Logo" /></a>
                                <a className="navbar-brand hidden" href="./"><img src="images/logotocky.png" alt="Logo" /></a>
                                <a id="menuToggle" className="menutoggle"><i className="fa fa-bars" /></a>
                            </div>
                        </div>
                        <div className="top-right">
                            <div className="header-menu">
                                <div className="header-left">
                                    <button className="search-trigger"><NavLink to="/"><i className="fas fa-home"></i></NavLink></button>
                                </div>
                            </div>
                        </div>
                    </header>
                    <div className="content">
                        {this.props.children}
                    </div>

                </div>
            </div>
        )
    }
}
