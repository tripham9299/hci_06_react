import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Gotocky from "./page/Gotocky/Gotocky";
import ChiTietQuyTac from "./page/ChiTietQuyTac/ChiTietQuyTac";
import LuyenTap from "./page/LuyenTap/LuyenTap";
import ChuyenDoiKyTu from "./page/ChuyenDoiKyTu/ChuyenDoiKyTu";
import QuanLyTuDien from "./page/QuanLyTuDien/QuanLyTuDien";
import ThongKe from "./page/ThongKe/ThongKe";
import HocQuyTac from './page/HocQuyTac/HocQuyTac';
import HocQuyTacChiTiet from './page/HocQuyTac/HocQuyTacChiTiet';
import LuyenTapGoMau from './page/LuyenTapGoMau/LuyenTapGoMau';
import LuyenGoTu from './page/LuyenGoTu/LuyenGoTu';
import LuyenGoCau from './page/LuyenGoCau/LuyenGoCau';
import LuyenGoDoan from './page/LuyenGoDoan/LuyenGoDoan';
import { ThongkeContext, dataMonth } from "./context/Context";
import { Component } from "react";
import HomePage from "./page/HomePage/HomePage";
class App extends Component {
  constructor(props) {
    super(props);

    this.setContextData = arr => {
      localStorage.setItem('arr', arr)
      this.setState({
        contextData: arr
      });
    };


    this.state = {
      contextData: localStorage.getItem('arr') != null ? localStorage.getItem('arr').split(',') : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      setContextData: this.setContextData,
    };
  }
  render() {
    return (
      <ThongkeContext.Provider value={this.state}>
        <Router>
          <Switch>
            <HomePage exact path="/" />
            <Layout>
              <Switch>
                <Gotocky exact path="/gotocky" />
                <ChiTietQuyTac exact path="/chitietquytac" />
                <LuyenTap exact path="/tracnghiemquytac" />
                <ChuyenDoiKyTu exact path="/chuyendoikytu" />
                <QuanLyTuDien exact path="/quanlutudien" />
                <ThongKe exact path="/thongke" />
                <HocQuyTac exact path='/hocquytac' />
                <LuyenTapGoMau exact path='/luyentap' />
                <LuyenGoTu exact path='/luyengotu' />
                <LuyenGoCau exact path='/luyengocau' />
                <LuyenGoDoan exact path='/luyengodoan' />
                <Route path="/hocquytac/:type" exact component={HocQuyTacChiTiet} />
              </Switch>
            </Layout>
          </Switch>


        </Router>
      </ThongkeContext.Provider>
    );
  }
}


export default App;
