import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import './HomePage.css'

export default class HomePage extends Component {
    render() {
        return (
            <div className="homepage" style={{ backgroundImage: 'url(images/homepage.jpg)' }}>
                <div className="overlay_background">
                    <div className="container">
                        <div className="row">
                            <div className="col-xl-6 col-lg-8 col-md-7 col-sm-8">
                                <div >
                                    {/* <span>#1 aritecture in united stare</span> */}
                                    <h1 className="titleTocky">TOCKY<br /> NEVER DIE</h1>
                                    <p style={{ color: '#fff' }}>Chúng tôi sẽ đem đến cho bạn trải nghiệm tuyệt vời nhất.</p>
                                    <a href="/gotocky" className="getStarted">BẮT ĐẦU</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
