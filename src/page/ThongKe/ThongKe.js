import React, { Component, useState } from "react";
import { Bar } from "react-chartjs-2";
import {ThongkeContext, dataMonth} from '../../context/Context'
class ThongKe extends Component{
        render(){
        // const data = {
        //     labels: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        //     datasets: [
        //       {
        //         label: "Điểm TB",
        //         backgroundColor: "rgba(255,99,132,0.2)",
        //         borderColor: "rgba(255,99,132,1)",
        //         borderWidth: 1,
        //         hoverBackgroundColor: "rgba(255,99,132,0.4)",
        //         hoverBorderColor: "rgba(255,99,132,1)",
        //         data: [5,9,7,3,6.5,9,7.6,5.3,9.4,6,8,2.9],
        //       },
        //     ],
        //   };
          return (
              <ThongkeContext.Consumer>
             { ({contextData, setContextData}) => (
                    <div>
              <Bar
                data={
                    {
                        labels: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
                        datasets: [
                          {
                            label: "Điểm TB",
                            backgroundColor: "rgba(255,99,132,0.2)",
                            borderColor: "rgba(255,99,132,1)",
                            borderWidth: 1,
                            hoverBackgroundColor: "rgba(255,99,132,0.4)",
                            hoverBorderColor: "rgba(255,99,132,1)",
                            data: contextData,
                          },
                        ],
                      }
                }
                width={1000}
                height={500}
                options={{
                  maintainAspectRatio: false,
                }}
              />
            </div>
             )
             } 
            
            </ThongkeContext.Consumer>
          );
            }
}
export default ThongKe
