import React, { Component } from 'react'

export default class QuanLyTuDien extends Component {
    render() {
        return (
            <div style={{ width: '100%' }}>
                <div className="animated fadeIn">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card">
                                <div className="card-header">
                                    <strong className="card-title">Quản lý bộ từ điển</strong>
                                </div>
                                <table id="bootstrap-data-table-1" className="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên</th>
                                            <th>Độ dài</th>
                                            <th>Ngày sửa</th>
                                            <th>Người sửa</th>
                                            <th>Trạng thái</th>
                                            <th>Hoạt động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td>Từ điển âm tiết v3</td>
                                            <td>	6900	</td>
                                            <td>	2020-09-10 13:40:09.0	</td>
                                            <td>	system	</td>
                                            <td>		</td>
                                            <td><a href="https://www.tiepcancntt.com:8888/test-manager-dict-steno-list.html?data=download&nameFile=dictVersion3.json"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
