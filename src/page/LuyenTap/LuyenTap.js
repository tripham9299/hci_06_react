import React, { Component } from "react";
import { Progress, Card, CardText, CardBody, Button } from "reactstrap";
import "./LuyenTap.css";
import classNames from "classnames";
import { ThongkeContext } from "../../context/Context";
const amtiet = [
  "ngh",
  "đ",
  "kh",
  "gh",
  "b",
  "gi",
  "c",
  "d",
  "ch",
  "g",
  "h",
  "k",
  "l",
  "m",
  "n",
  "p",
  "q",
  "r",
  "s",
  "t",
  "th",
  "v",
  "ph",
  "x",
  "ng",
  "nh",
  "tr",
  "uổ",
  "uỗ",
  "ă",
  "uộ",
  "uớ",
  "uỳa",
  "uờ",
  "uở",
  "uỡ",
  "uâ",
  "uợ",
  "oạ",
  "ía",
  "oả",
  "uê",
  "ửa",
  "ia",
  "ũa",
  "uỳ",
  "uô",
  "ụa",
  "oắ",
  "uỵ",
  "oằ",
  "uỷ",
  "oẳ",
  "uỹ",
  "oẵ",
  "oặ",
  "uý",
  "ĩ",
  "oẹ",
  "ướ",
  "oẻ",
  "ườ",
  "oẽ",
  "ưở",
  "ưỡ",
  "ượ",
  "uýa",
  "uya",
  "oọ",
  "uỵa",
  "ữa",
  "oà",
  "oá",
  "oã",
  "ĩa",
  "oè",
  "ủa",
  "oé",
  "a",
  "oò",
  "oó",
  "e",
  "yế",
  "yề",
  "i",
  "ũ",
  "yể",
  "yễ",
  "yệ",
  "o",
  "oă",
  "u",
  "iế",
  "iề",
  "y",
  "uỷa",
  "iể",
  "iễ",
  "iệ",
  "ùa",
  "ua",
  "yê",
  "ựa",
  "uyế",
  "ứa",
  "uyề",
  "uyể",
  "uyễ",
  "iê",
  "ơ",
  "ạ",
  "uyệ",
  "ả",
  "uy",
  "ấ",
  "ầ",
  "úa",
  "ẩ",
  "ẫ",
  "ậ",
  "ắ",
  "ư",
  "ưa",
  "ằ",
  "ẳ",
  "ẵ",
  "ặ",
  "uỹa",
  "ẹ",
  "ẻ",
  "ỉa",
  "ẽ",
  "ế",
  "ề",
  "ể",
  "ễ",
  "ệ",
  "uyê",
  "ỉ",
  "ị",
  "uơ",
  "ọ",
  "ỏ",
  "uấ",
  "ố",
  "oa",
  "uầ",
  "ồ",
  "uẩ",
  "ừa",
  "ổ",
  "oe",
  "uẫ",
  "ỗ",
  "uậ",
  "ộ",
  "ớ",
  "ờ",
  "ở",
  "à",
  "oo",
  "ỡ",
  "á",
  "â",
  "ợ",
  "ã",
  "ụ",
  "ủ",
  "è",
  "é",
  "ứ",
  "ê",
  "uế",
  "ừ",
  "ì",
  "uề",
  "í",
  "ử",
  "uể",
  "ữ",
  "uễ",
  "ự",
  "ươ",
  "ò",
  "uệ",
  "ỳ",
  "ó",
  "ô",
  "ỵ",
  "ìa",
  "ịa",
  "õ",
  "ỷ",
  "ỹ",
  "ù",
  "ú",
  "uố",
  "ý",
  "uồ",
  "p",
  "c",
  "t",
  "ch",
  "u",
  "ng",
  "i",
  "y",
  "nh",
  "m",
  "n",
  "o",
];
const phimSteno = [
  "KR",
  "TP",
  "KH",
  "KP",
  "P",
  "SH",
  "K",
  "SP",
  "SK",
  "KP",
  "H",
  "K",
  "SR",
  "RH",
  "PR",
  "SPH",
  "STK",
  "R",
  "S",
  "T",
  "TH",
  "SPR",
  "PH",
  "ST",
  "KR",
  "TK",
  "TR",
  "-HSUO",
  "N-SUO",
  "*-A",
  "N-UO",
  "-SY",
  "-HEY",
  "-HY",
  "-HSY",
  "N-SY",
  "*-Y",
  "N-Y",
  "N-U",
  "-SIE",
  "-HSU",
  "-UE",
  "-HSEW",
  "-IE",
  "N-SUO",
  "-HUY",
  "-UO",
  "N-UO",
  "-S*U",
  "N-UY",
  "-H*U",
  "-HSUY",
  "-HS*U",
  "N-SUY",
  "N-S*U",
  "N*-U",
  "-SUY",
  "N-S*I",
  "N-AY",
  "-SEW",
  "-HSAY",
  "-HEW",
  "N-SAY",
  "-HSEW",
  "N-SEW",
  "N-EW",
  "-SEY",
  "-EY",
  "N*-O",
  "N-EY",
  "N-SEW",
  "-HU",
  "-SU",
  "N-SU",
  "N-SIE",
  "-HAY",
  "-HSUO",
  "-SAY",
  "-A",
  "-H*O",
  "-S*O",
  "-E",
  "-SIE",
  "-HIE",
  "*-I",
  "N-SIU",
  "-HSIE",
  "N-SIE",
  "N-IE",
  "-O",
  "*-U",
  "-IU",
  "-SIE",
  "-HIE",
  "-I",
  "-HSEY",
  "-HSIE",
  "N-SIE",
  "N-IE",
  "-HUO",
  "-UO",
  "-IE",
  "N-EW",
  "-SEY",
  "-SEW",
  "-HEY",
  "-HSEY",
  "N-SEY",
  "-IE",
  "-W",
  "N-A",
  "N-EY",
  "-HSA",
  "-UY",
  "-S*W",
  "-H*W",
  "-SUO",
  "-HS*W",
  "N-S*W",
  "N*-W",
  "-S*A",
  "-UW",
  "-EW",
  "-H*A",
  "-HS*A",
  "N-S*A",
  "N*-A",
  "N-SEY",
  "N-E",
  "-HSE",
  "-HSIE",
  "N-SE",
  "-SOE",
  "-HOE",
  "-HSOE",
  "N-SOE",
  "N-OE",
  "-EY",
  "-HS*I",
  "N*-I",
  "-Y",
  "N-O",
  "-HSO",
  "-S*Y",
  "-SOW",
  "-U",
  "-H*Y",
  "-HOW",
  "-HS*Y",
  "-HEW",
  "-HSOW",
  "-AY",
  "N-S*Y",
  "N-SOW",
  "N*-Y",
  "N-OW",
  "-SW",
  "-HW",
  "-HSW",
  "-HA",
  "*-O",
  "N-SW",
  "-SA",
  "*-W",
  "N-W",
  "N-SA",
  "N-IU",
  "-HSIU",
  "-HE",
  "-SE",
  "-SUW",
  "-OE",
  "-SUE",
  "-HUW",
  "-H*I",
  "-HUE",
  "-S*I",
  "-HSUW",
  "-HSUE",
  "N-SUW",
  "N-SUE",
  "N-UW",
  "-EW",
  "-HO",
  "N-UE",
  "-HI",
  "-SO",
  "-OW",
  "N-I",
  "-HIE",
  "N-IE",
  "N-SO",
  "-HSI",
  "N-SI",
  "-HIU",
  "-SIU",
  "-SUO",
  "-SI",
  "-HUO",
  "-TK",
  "-NK",
  "-NT",
  "-GK",
  "-JG",
  "-G",
  "-J",
  "-JN",
  "-K",
  "-T",
  "-N",
  "-JK",
];
const wrongAnswer = [
  "BP",
  "DG",
  "HS",
  "*GH",
  "-HK",
  "-GHF",
  "*BC",
  "GN",
  "KL",
  "-RS",
  "-*HN",
  "BN*",
  "*JKB",
  "-BP",
  "*KL",
  "N-SC",
  "N-SD",
  "GH",
];
export default class LuyenTap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardSelected: false,
      answerSelected: null,

      numberRandomQuestion: null,
      randomQuestion: null,
      answer: null,
      numberCorrectAnswer: null,
      wrong1: null,
      wrong2: null,
      wrong3: null,

      footerCheck: null,
      countProgress: 1,
      countQuestion: 1,

      totalCorrect: 0,
      totalWrong: 0,

      complete: false,
    };
  }
  handleSelectAnswer1 = () => {
    this.setState({
      cardSelected: true,
      answerSelected: 1,
    });
  };
  handleSelectAnswer2 = () => {
    this.setState({
      cardSelected: true,
      answerSelected: 2,
    });
  };
  handleSelectAnswer3 = () => {
    this.setState({
      cardSelected: true,
      answerSelected: 3,
    });
  };
  handleSelectAnswer4 = () => {
    this.setState({
      cardSelected: true,
      answerSelected: 4,
    });
  };
  handleCheck = () => {
    if (this.state.answerSelected === this.state.numberCorrectAnswer) {
      this.setState({
        totalCorrect: this.state.totalCorrect + 1,
        footerCheck: true,
        countProgress: this.state.countProgress + 1,
      });
    } else {
      this.setState({
        totalWrong: this.state.totalWrong + 1,
        footerCheck: false,
        countProgress: this.state.countProgress + 1,
      });
    }
  };

  handleNext = () => {
    let numberRandomQuestion = Math.floor(Math.random() * 211);
    let randomQuestion = amtiet[numberRandomQuestion];
    let answer = phimSteno[numberRandomQuestion];
    let numberCorrectAnswer = Math.floor(Math.random() * 4 + 1);
    var numberWrong1 = 0;
    var numberWrong2 = 0;
    var numberWrong3 = 0;
    while (
      numberWrong1 === numberWrong2 ||
      numberWrong2 === numberWrong3 ||
      numberWrong1 === numberWrong3
    ) {
      numberWrong1 = Math.floor(Math.random() * 18);
      numberWrong2 = Math.floor(Math.random() * 18);
      numberWrong3 = Math.floor(Math.random() * 18);
    }
    let wrong1 = wrongAnswer[numberWrong1];
    let wrong2 = wrongAnswer[numberWrong2];
    let wrong3 = wrongAnswer[numberWrong3];

    this.setState({
      answerSelected: null,
      footerCheck: null,
      cardSelected: false,
      numberRandomQuestion: numberRandomQuestion,
      randomQuestion: randomQuestion,
      answer: answer,
      numberCorrectAnswer: numberCorrectAnswer,
      wrong1: wrong1,
      wrong2: wrong2,
      wrong3: wrong3,
      countQuestion: this.state.countQuestion + 1,
    });
  };

  handleDismiss = () => {
    if (this.state.countQuestion < 10) {
      let numberRandomQuestion = Math.floor(Math.random() * 211);
      let randomQuestion = amtiet[numberRandomQuestion];
      let answer = phimSteno[numberRandomQuestion];
      let numberCorrectAnswer = Math.floor(Math.random() * 4 + 1);
      var numberWrong1 = 0;
      var numberWrong2 = 0;
      var numberWrong3 = 0;
      while (
        numberWrong1 === numberWrong2 ||
        numberWrong2 === numberWrong3 ||
        numberWrong1 === numberWrong3
      ) {
        numberWrong1 = Math.floor(Math.random() * 18);
        numberWrong2 = Math.floor(Math.random() * 18);
        numberWrong3 = Math.floor(Math.random() * 18);
      }
      let wrong1 = wrongAnswer[numberWrong1];
      let wrong2 = wrongAnswer[numberWrong2];
      let wrong3 = wrongAnswer[numberWrong3];

      this.setState({
        countProgress: this.state.countProgress + 1,
        totalWrong: this.state.totalWrong + 1,
        answerSelected: null,
        footerCheck: null,
        cardSelected: false,
        numberRandomQuestion: numberRandomQuestion,
        randomQuestion: randomQuestion,
        answer: answer,
        numberCorrectAnswer: numberCorrectAnswer,
        wrong1: wrong1,
        wrong2: wrong2,
        wrong3: wrong3,
        countQuestion: this.state.countQuestion + 1,
      });
    } else {
      this.setState({
        totalWrong: this.state.totalWrong + 1,
        complete: true,
      });
    }
  };

  handleComplete = () => {
    this.setState({
      complete: true,
    });
  };

  handleOnceMore = () => {

    let numberRandomQuestion = Math.floor(Math.random() * 211);
    let randomQuestion = amtiet[numberRandomQuestion];
    let answer = phimSteno[numberRandomQuestion];
    let numberCorrectAnswer = Math.floor(Math.random() * 4 + 1);
    var numberWrong1 = 0;
    var numberWrong2 = 0;
    var numberWrong3 = 0;
    while (
      numberWrong1 === numberWrong2 ||
      numberWrong2 === numberWrong3 ||
      numberWrong1 === numberWrong3
    ) {
      numberWrong1 = Math.floor(Math.random() * 18);
      numberWrong2 = Math.floor(Math.random() * 18);
      numberWrong3 = Math.floor(Math.random() * 18);
    }
    let wrong1 = wrongAnswer[numberWrong1];
    let wrong2 = wrongAnswer[numberWrong2];
    let wrong3 = wrongAnswer[numberWrong3];

    this.setState({
      show: '',
      answerSelected: null,
      complete: false,
      countProgress: 1,
      countQuestion: 1,

      totalCorrect: 0,
      totalWrong: 0,
      footerCheck: null,
      cardSelected: false,
      numberRandomQuestion: numberRandomQuestion,
      randomQuestion: randomQuestion,
      answer: answer,
      numberCorrectAnswer: numberCorrectAnswer,
      wrong1: wrong1,
      wrong2: wrong2,
      wrong3: wrong3,
    });
  };

  componentDidMount = () => {
    let numberRandomQuestion = Math.floor(Math.random() * 211);
    let randomQuestion = amtiet[numberRandomQuestion];
    let answer = phimSteno[numberRandomQuestion];
    let numberCorrectAnswer = Math.floor(Math.random() * 4 + 1);
    var numberWrong1 = 0;
    var numberWrong2 = 0;
    var numberWrong3 = 0;
    while (
      numberWrong1 === numberWrong2 ||
      numberWrong2 === numberWrong3 ||
      numberWrong1 === numberWrong3
    ) {
      numberWrong1 = Math.floor(Math.random() * 18);
      numberWrong2 = Math.floor(Math.random() * 18);
      numberWrong3 = Math.floor(Math.random() * 18);
    }
    let wrong1 = wrongAnswer[numberWrong1];
    let wrong2 = wrongAnswer[numberWrong2];
    let wrong3 = wrongAnswer[numberWrong3];

    this.setState({
      numberRandomQuestion: numberRandomQuestion,
      randomQuestion: randomQuestion,
      answer: answer,
      numberCorrectAnswer: numberCorrectAnswer,
      wrong1: wrong1,
      wrong2: wrong2,
      wrong3: wrong3,
    });
  };
  render() {
    const {
      cardSelected,
      randomQuestion,
      numberCorrectAnswer,
      answer,
      wrong1,
      wrong2,
      wrong3,
      footerCheck,
      countProgress,
      answerSelected,
      countQuestion,
      complete,
      totalCorrect,
      totalWrong,
    } = this.state;
    return (
      <ThongkeContext.Consumer>
        {({ contextData, setContextData }) => (
          <div style={{ width: '100%' }}>
            {complete === false && (
              <div style={{ width: "100%" }}>
                <div className="progress_practice">
                  <Progress
                    className="pro"
                    color="success"
                    value={(countProgress - 1) * 10}
                  >
                    <div className="progressChild"></div>
                  </Progress>
                </div>
                <h4 className="question">
                  Câu hỏi {countQuestion} : Đâu là phím steno tương ứng cho âm "
                  {randomQuestion}"?
                </h4>
                <div
                  className="container-fluid"
                  style={{ marginBottom: "40px" }}
                >
                  {numberCorrectAnswer === 1 && (
                    <div className="row ">
                      <div className="col-md-6 ">
                        <Card
                          onClick={this.handleSelectAnswer1}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 1,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{answer}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer2}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 2,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong1}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer3}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 3,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong2}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer4}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 4,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong3}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                    </div>
                  )}

                  {numberCorrectAnswer === 2 && (
                    <div className="row">
                      <div className="col-md-6 ">
                        <Card
                          onClick={this.handleSelectAnswer1}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 1,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong1}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer2}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 2,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{answer}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer3}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 3,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong2}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer4}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 4,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong3}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                    </div>
                  )}

                  {numberCorrectAnswer === 3 && (
                    <div className="row">
                      <div className="col-md-6 ">
                        <Card
                          onClick={this.handleSelectAnswer1}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 1,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong1}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer2}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 2,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong2}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer3}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 3,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{answer}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer4}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 4,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong3}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                    </div>
                  )}

                  {numberCorrectAnswer === 4 && (
                    <div className="row">
                      <div className="col-md-6 ">
                        <Card
                          onClick={this.handleSelectAnswer1}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 1,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong1}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer2}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 2,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong2}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer3}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 3,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{wrong3}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                      <div className="col-md-6">
                        <Card
                          onClick={this.handleSelectAnswer4}
                          className={classNames("cardAnswer", {
                            cardAnswerFocus: answerSelected === 4,
                          })}
                        >
                          <CardBody
                            style={{
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "center",
                              height: "90px",
                            }}
                          >
                            <CardText>{answer}</CardText>
                          </CardBody>
                        </Card>
                      </div>
                    </div>
                  )}
                </div>
                {footerCheck === null && (
                  <div className="row checkBottom">
                    <div
                      className="col-md-6"
                      style={{ display: "flex", justifyContent: "center" }}
                    >
                      <Button type="submit" onClick={this.handleDismiss}>
                        Bỏ Qua
                      </Button>
                    </div>
                    <div
                      className="col-md-6"
                      style={{ display: "flex", justifyContent: "center" }}
                    >
                      {cardSelected === false && (
                        <Button type="submit">Kiểm tra</Button>
                      )}
                      {cardSelected === true && (
                        <Button
                          type="submit"
                          onClick={this.handleCheck}
                          style={{ backgroundColor: "#58a700", color: "#fff" }}
                        >
                          Kiểm tra
                        </Button>
                      )}
                    </div>
                  </div>
                )}
                {footerCheck === true && (
                  <div className="row checkBottom Correct">
                    <div
                      className="col-md-6"
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <i
                        className="fas fa-check-circle"
                        style={{ color: "#fff", fontSize: "70px" }}
                      ></i>
                      <p>Chính xác!</p>
                    </div>
                    {countQuestion < 10 && (
                      <div
                        className="col-md-6"
                        style={{ display: "flex", justifyContent: "center" }}
                      >
                        <Button
                          type="submit"
                          onClick={this.handleNext}
                          style={{ backgroundColor: "#58a700", color: "#fff" }}
                        >
                          Tiếp tục
                        </Button>
                      </div>
                    )}
                    {countQuestion === 10 && (
                      <div
                        className="col-md-6"
                        style={{ display: "flex", justifyContent: "center" }}
                      >
                        <Button
                          type="submit"
                          onClick={this.handleComplete}
                          style={{ backgroundColor: "#58a700", color: "#fff" }}
                        >
                          Hoàn Thành
                        </Button>
                      </div>
                    )}
                  </div>
                )}
                {footerCheck === false && (
                  <div className="row checkBottom Wrong ">
                    <div
                      className="col-md-6"
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <i
                        class="fas fa-times-circle"
                        style={{ color: "#fff", fontSize: "70px" }}
                      ></i>
                      <p>Đáp án đúng:</p>
                      <p>{answer}</p>
                    </div>
                    {countQuestion < 10 && (
                      <div
                        className="col-md-6"
                        style={{ display: "flex", justifyContent: "center" }}
                      >
                        <Button
                          type="submit"
                          onClick={this.handleNext}
                          style={{ backgroundColor: "#ea2b2b", color: "#fff" }}
                        >
                          Tiếp tục
                        </Button>
                      </div>
                    )}
                    {countQuestion === 10 && (
                      <div
                        className="col-md-6"
                        style={{ display: "flex", justifyContent: "center" }}
                      >
                        <Button
                          type="submit"
                          onClick={this.handleComplete}
                          style={{ backgroundColor: "#58a700", color: "#fff" }}
                        >
                          Hoàn Thành
                        </Button>
                      </div>
                    )}
                  </div>
                )}
              </div>
            )}
            {complete === true && (
              <div className="complete">
                <div className="row">
                  <div className="col-md-3"></div>
                  <div className="col-md-6">
                    <h3>Chúc mừng bạn đã hoàn thành!</h3>
                    <div className="row">
                      <div className="col-sm-6">
                        <p>Số câu đúng: {totalCorrect}</p>
                        <p>Số câu sai: {totalWrong}</p>
                        <Button
                          style={{ backgroundColor: "#58a700" }}
                          onClick={this.handleOnceMore}
                          className="onceMore"
                        >
                          Chơi lại
                        </Button>
                        <Button
                          style={{ backgroundColor: "#58a700", display: `${this.state.show}` }}
                          onClick={() => {
                            let arr = contextData;
                            let d = new Date();
                            let month = parseInt(d.getMonth());
                            arr[month] == 0 ? arr[month] = this.state.totalCorrect : arr[month] = (this.state.totalCorrect + arr[month]) / 2;

                            setContextData(arr);
                            setContextData(arr);
                            this.setState({ show: 'none' })
                          }}
                          className="onceMore"
                        >
                          Xác nhận điểm vào hệ thống
                        </Button>

                      </div>
                      <div className="col-sm-6">
                        <img
                          src="images/panda.png"
                          style={{ maxWidth: "180px", marginLeft: '70px' }}
                          alt="panda"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3"></div>
                </div>
              </div>
            )}
          </div>
        )}
      </ThongkeContext.Consumer>
    );
  }
}
