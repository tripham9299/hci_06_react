import React, { Component, useRef, useState } from 'react'
import "./Gotocky.css"
import { amdau, amcuoi, amchinh, convertTocky, converInputTocky, converInputTockyRight } from './../../convert/quytac.js'


const getTockyValid = (tocky) => {
    return converInputTocky(tocky)
}

export default function Gotocky(props) {

    const [tocky, setTocky] = useState("")
    const [tockyValid, setTockyValid] = useState("")
    const typingTimeoutRef = useRef(null);
    const [dangGo, setDangGo] = useState('')
    const [tockyDanggo, setTockyDango] = useState("Lưu ý: Tắt phần mềm hỗ trợ gõ tiếng việt khi sử dụng gõ tốc ký ")
    const timeOld = useRef(0);



    const handleChangeTocky = (e) => {
        let value = e.target.value;
        value = value[value.length - 1]

        let newDangGo = dangGo + value

        let kytuTockyDango = converInputTockyRight(newDangGo)
        let time = new Date();
        let timeFormat = `${time.getHours()}:${time.getMinutes()}:${time.getMilliseconds()}`
        setTockyDango(timeFormat + "  " + kytuTockyDango + `\n` + tockyDanggo)
        setDangGo(newDangGo)


        if (typingTimeoutRef.current) {
            clearTimeout(typingTimeoutRef.current);
        }

        typingTimeoutRef.current = setTimeout(() => {
            let newTockyValid = getTockyValid(newDangGo);
            if (newTockyValid) setTockyValid(tockyValid + ' ' + newTockyValid)
            setDangGo('')
        }, 1000)

        setTocky('')

    }

    return (
        <div className="content">
            <div id="container">
                <label htmlFor>Văn bản:</label>
                <textarea id="write" rows={6} cols={60} onChange={handleChangeTocky} value={tockyValid + tocky} defaultValue={""} />
                <ul id="keyboard">
                    <li className="symbol"><span className="off">`</span><span className="on">~</span></li>
                    <li className="symbol"><span className="off">1</span><span className="on">!</span></li>
                    <li className="symbol"><span className="off">2</span><span className="on">@</span></li>
                    <li className="symbol"><span className="off">3</span><span className="on">#</span></li>
                    <li className="symbol"><span className="off">4</span><span className="on">$</span></li>
                    <li className="symbol"><span className="off">5</span><span className="on">%</span></li>
                    <li className="symbol"><span className="off">6</span><span className="on">^</span></li>
                    <li className="symbol"><span className="off">7</span><span className="on">&amp;</span></li>
                    <li className="symbol"><span className="off">8</span><span className="on">*</span></li>
                    <li className="symbol"><span className="off">9</span><span className="on">(</span></li>
                    <li className="symbol"><span className="off">0</span><span className="on">)</span></li>
                    <li className="symbol"><span className="off">-</span><span className="on">_</span></li>
                    <li className="symbol"><span className="off">=</span><span className="on">+</span></li>
                    <li className="delete lastitem">Delete</li>
                    <li className="tab">Tab</li>
                    <li className="letter">Q</li>
                    <li className="letter">W</li>
                    <li className="letter">E</li>
                    <li className="letter">R</li>
                    <li className="letter">T</li>
                    <li className="letter">Y</li>
                    <li className="letter">U</li>
                    <li className="letter">I</li>
                    <li className="letter">O</li>
                    <li className="letter">P</li>
                    <li className="symbol"><span className="off">[</span><span className="on">{'{'}</span></li>
                    <li className="symbol"><span className="off">]</span><span className="on">{'}'}</span></li>
                    <li className="symbol lastitem"><span className="off">\</span><span className="on">|</span></li>
                    <li className="capslock">Caps lock</li>
                    <li className="letter">A</li>
                    <li className="letter">S</li>
                    <li className="letter">D</li>
                    <li className="letter">F</li>
                    <li className="letter">G</li>
                    <li className="letter">H</li>
                    <li className="letter">J</li>
                    <li className="letter">K</li>
                    <li className="letter">L</li>
                    <li className="symbol"><span className="off">;</span><span className="on">:</span></li>
                    <li className="symbol"><span className="off">'</span><span className="on">"</span></li>
                    <li className="return lastitem">Enter</li>
                    <li className="left-shift">Shift</li>
                    <li className="letter">Z</li>
                    <li className="letter">X</li>
                    <li className="letter">C</li>
                    <li className="letter">V</li>
                    <li className="letter">B</li>
                    <li className="letter">N</li>
                    <li className="letter">M</li>
                    <li className="symbol"><span className="off">,</span><span className="on">&lt;</span></li>
                    <li className="symbol"><span className="off">.</span><span className="on">&gt;</span></li>
                    <li className="symbol"><span className="off">/</span><span className="on">?</span></li>
                    <li className="right-shift lastitem">Shift</li>
                    <li className="space lastitem">&nbsp;</li>
                </ul>
            </div>
            <div id="container2">
                <label htmlFor>Tốc ký:</label>
                <textarea id="write2" rows={6} cols={60} value={tockyDanggo} />
                <ul id="keyboard2">

                    <li className="space_firstitem"></li>
                    <li className="letter2">S</li>
                    <li className="letter2">K</li>
                    <li className="letter2">R</li>
                    <li className="letter2">N</li>
                    <li className="letter2">H</li>
                    <li className="letter2">*</li>
                    <li className="letter2">W</li>
                    <li className="letter2">J</li>
                    <li className="letter2">N</li>
                    <li className="letter2">T</li>
                    <li className="letter2">T</li>
                    <li className="letter2">P</li>
                    <li className="letter2">H</li>
                    <li className="letter2">N</li>
                    <li className="letter2">S</li>
                    <li className="letter2">I</li>
                    <li className="letter2">Y</li>
                    <li className="letter2">J</li>
                    <li className="letter2">G</li>
                    <li className="letter2">K</li>
                    <li className="letter2" style={{ marginLeft: '150px' }}>U</li>
                    <li className="letter2">O</li>
                    <li className="letter2">E</li>
                    <li className="letter2">A</li>

                </ul>
            </div>
        </div>
    )

}
