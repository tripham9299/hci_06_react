import React, { useState } from "react";
import "./ChuyenDoiKyTu.css";
import { amdau, amcuoi, amchinh, convertTocky } from './../../convert/quytac.js'
import { Button } from 'reactstrap';

export default function App() {
  const [originText, setOriginText] = useState("");
  const [convertedText, setConvertedText] = useState("");
  const [flexDirectionAttr, setflexDirectionAttr] = useState("column");
  function handleChange(e) {
    let value = e.target.value;
    setOriginText(value);
    let result = value.trim().split(' ').map((word) => convertTocky(word)).join(' ')
    setConvertedText(result)
  }

  function onClickConvertTocky() {
    let result = originText.trim().split(' ').map((word) => convertTocky(word)).join(' ')
    setConvertedText(result)
  }


  return (

    <div className="convert">
      <h4 style={{ marginBottom: '50px' }}>Chuyển đổi ký tự</h4>
      <div className="row" style={{ marginBottom: '50px' }}>
        <div className="col-md-5">
          <div >
            <label style={{ fontWeight: 'bold' }}>Origin</label>
            <textarea
              className="originText"
              rows={20}
              style={{ width: '100%' }}
              // cols={100}
              value={originText}
              onChange={handleChange}
            ></textarea>
          </div>
        </div>
        <div className="col-md-2 buttonConvert"><Button color="primary" onClick={onClickConvertTocky}><i className="fas fa-arrows-alt-h"></i></Button></div>
        <div className="col-md-5">
          <div>
            <label style={{ fontWeight: 'bold' }}>Tốc ký</label>
            <textarea
              style={{ width: '100%' }}
              className="chuyendoiedText"
              value={convertedText}
              rows={20}
            // onChange={convertToOrigin}
            ></textarea>
          </div>
        </div>
      </div>



    </div>
  );
}
