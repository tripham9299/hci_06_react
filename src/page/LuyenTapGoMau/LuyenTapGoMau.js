import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import "./LuyenTapGoMau.css";

export default class LuyenTapGoMau extends Component {
    render() {
        return (
            <div >
                <div style={{ marginBottom: '30px', fontSize: '30px' }}><Link className="LuyenTapHome" to="/luyengotu" ><i className="fas fa-dice-five" style={{ marginRight: '35px', color: '#fc8c03' }}></i>Luyện gõ từ</Link></div>
                <div style={{ marginBottom: '30px', fontSize: '30px' }}><Link className="LuyenTapHome" to="/luyengocau" ><i className="fas fa-dice" style={{ marginRight: '23px', color: '#1f62cf' }}></i>Luyện gõ câu</Link></div>
                <div style={{ marginBottom: '30px', fontSize: '30px' }}><Link className="LuyenTapHome" to="/luyengodoan" ><i className="fas fa-chess-queen" style={{ marginRight: '31px', color: '#07b510' }}></i>Luyện gõ đoạn</Link></div>
            </div>
        )
    }
}
