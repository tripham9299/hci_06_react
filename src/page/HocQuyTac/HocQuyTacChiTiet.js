import React, { useState } from 'react'
import html from './HtmlData.js'

export default function HocQuyTacChiTiet(props) {
    const type = props.match.params.type;
    console.log(type);
    const [data, setData] = useState('')
    const htmldata = html[type]
    console.log("html length: " + htmldata.length)
    return (
        <div>
            <div dangerouslySetInnerHTML={{ __html: htmldata }} />
        </div>
    )
}

