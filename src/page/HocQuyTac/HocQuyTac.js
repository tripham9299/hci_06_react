import React from 'react'
import { Link } from 'react-router-dom'
import './HocQuyTac.css'
export default function HocQuyTac() {
    return (
        <div className="hocquytac row">

            <div class="card text-center col-sm-4">

                <div class="card-body">
                    <h4 class="card-title">Học âm đầu</h4>
                    <p>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    </p>
                    <div class="progress-row">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped" style={{ width: "85%" }}>85%</div>
                        </div>
                    </div>
                    {/* <p class="card-text ">Some example text.</p> */}
                    <br />
                    <br />
                    <div class="text-center">
                        <Link to="/hocquytac/amdau" class="btn btn-success ">Chi tiết</Link>

                    </div>
                </div>
            </div>


            <div class="card text-center col-sm-4">

                <div class="card-body">
                    <h4 class="card-title">Học âm chính</h4>
                    <p>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    </p>
                    <div class="progress-row">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped" style={{ width: "55%" }}>55%</div>
                        </div>
                    </div>
                    {/* <p class="card-text ">Some example text.</p> */}
                    <br />
                    <br />
                    <div class="text-center">
                        <Link to="/hocquytac/amchinh" class="btn btn-success ">Chi tiết</Link>

                    </div>
                </div>
            </div>

            <div class="card text-center col-sm-4">

                <div class="card-body">
                    <h4 class="card-title">Học âm cuối</h4>
                    <p>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    </p>
                    <div class="progress-row">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped" style={{ width: "40%" }}>40%</div>
                        </div>
                    </div>
                    {/* <p class="card-text ">Some example text.</p> */}
                    <br />
                    <br />
                    <div class="text-center">
                        <Link to="/hocquytac/amcuoi" class="btn btn-success ">Chi tiết</Link>

                    </div>
                </div>
            </div>


            <div class="card text-center col-sm-4">

                <div class="card-body">
                    <h4 class="card-title">Học quy tắc gõ số</h4>
                    <p>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    </p>
                    <div class="progress-row">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped" style={{ width: "60%" }}>60%</div>
                        </div>
                    </div>
                    {/* <p class="card-text ">Some example text.</p> */}
                    <br />
                    <br />
                    <div class="text-center">
                        <Link to="/hocquytac/amdau" class="btn btn-success ">Chi tiết</Link>

                    </div>
                </div>
            </div>


            <div class="card text-center col-sm-4">

                <div class="card-body">
                    <h4 class="card-title">Học quy tắc ký hiệu đặc biệt</h4>
                    <p>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    </p>
                    <div class="progress-row">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped" style={{ width: "90%" }}>90%</div>
                        </div>
                    </div>
                    {/* <p class="card-text ">Some example text.</p> */}
                    <br />
                    <br />
                    <div class="text-center">
                        <Link to="/hocquytac/amchinh" class="btn btn-success ">Chi tiết</Link>

                    </div>
                </div>
            </div>

            <div class="card text-center col-sm-4">

                <div class="card-body">
                    <h4 class="card-title">Học quy tắc xoá</h4>
                    <p>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    </p>
                    <div class="progress-row">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped" style={{ width: "30%" }}>30%</div>
                        </div>
                    </div>
                    {/* <p class="card-text ">Some example text.</p> */}
                    <br />
                    <br />
                    <div class="text-center">
                        <Link to="/hocquytac/amcuoi" class="btn btn-success ">Chi tiết</Link>

                    </div>
                </div>
            </div>
        </div>

    )
}
